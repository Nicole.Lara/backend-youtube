import { Test, TestingModule } from '@nestjs/testing';
import { SearchList } from '../context/user/aplication/searchList';
import { AppController } from './app.controller';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [SearchList],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getSearch('2')).toBe('Hello World!');
    });
  });
});
