import { Controller, Get, Param } from '@nestjs/common';
import { SearchList } from '../context/user/aplication/searchList';

@Controller('search')
export class AppController {
  constructor(private readonly searchList: SearchList) {}

  @Get('list/:searchterm')
  getSearch(@Param('searchterm') searchterm: string): Promise<string> {
    return this.searchList.run(searchterm);
  }
}
