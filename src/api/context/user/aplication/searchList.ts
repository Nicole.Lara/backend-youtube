import { Injectable } from '@nestjs/common';
import fetch from 'node-fetch';

@Injectable()
export class SearchList {
  async run(searchterm): Promise<any> {
    try {
      const response = await fetch(
        `https://youtube.googleapis.com/youtube/v3/search?channelType=any&key=AIzaSyCRfP1vl1vxrPKgi7SVwMdgRpRhtjgbVnY&part=snippet&q=${searchterm}`,
      );
      const data = response.json();
      return data;
    } catch (error) {
      throw error;
    }
  }
}
