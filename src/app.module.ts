import { Module } from '@nestjs/common';
import { SearchList } from './api/context/user/aplication/searchList';
import { AppController } from './api/controller/app.controller';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [SearchList],
})
export class AppModule {}
